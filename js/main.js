(function() {
  var ordertab = document.getElementById('og-orders-tab'),
      subtab = document.getElementById('og-subs-tab'),
      subRows = document.getElementsByClassName('og-sub-row'),
      addedEventListeners = false;
  
  ordertab.addEventListener('click', function(event) {
    tabChanged(event);
    if (addedEventListeners) {
      for(var i = 0; i < subRows.length; i++) {
        subRows[i].getElementsByClassName('og-subs-more-changes-toggle')[0].removeEventListener('click', handleClickToggleButton);
        if (subRows[i].getElementsByClassName('og-more-changes-container')[0].className.split(' ').indexOf('selected') >= 0) {
          toggleSelectedClass(subRows[i].getElementsByClassName('og-more-changes-container')[0]);
        }
      }
      addedEventListeners = false;
    } 
  });

  subtab.addEventListener('click', function(event) {
    tabChanged(event);
    addedEventListeners = true;
    for(var i = 0; i < subRows.length; i++) {
      subRows[i].getElementsByClassName('og-subs-more-changes-toggle')[0].addEventListener('click', handleClickToggleButton);
    }
  });

  function toggleSelectedClass (element) {
    if (element.classList) { 
      element.classList.toggle('selected');
    } else {
      var classes = element.className.split(' ');
      var i = classes.indexOf('selected');

      if (i >= 0) {
        classes.splice(i, 1);
      } else {
        classes.push('selected'); 
      }
      element.className = classes.join(' ');
    }
  }

  function tabChanged (event) {
    if (event.target.className.split(' ').indexOf('selected') == -1) {
      toggleSelectedClass(ordertab);
      toggleSelectedClass(subtab);

      toggleSelectedClass(document.getElementById('og-orders-template'));
      toggleSelectedClass(document.getElementById('og-subs-template'));
    }
  }

  function handleClickToggleButton (event) {
    var closest = function(el, fn) {
      return el && (fn(el) ? el : closest(el.parentNode, fn));
    }

    var subRow = closest(event.target, function(el) {
      if (el.className && el.className.split(' ').indexOf('og-sub-row') >= 0) {
        return el;
      } 
    });
    toggleSelectedClass(subRow.getElementsByClassName('og-more-changes-container')[0]);
  }
})();

